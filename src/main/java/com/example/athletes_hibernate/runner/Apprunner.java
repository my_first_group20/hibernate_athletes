package com.example.athletes_hibernate.runner;

import com.example.athletes_hibernate.model.Athlete;
import com.example.athletes_hibernate.model.Coach;
import com.example.athletes_hibernate.repository.AthleteRepository;
import com.example.athletes_hibernate.repository.CoachRepository;
import com.example.athletes_hibernate.service.AthleteService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
//import org.springframework.transaction.annotation.Transactional;
import javax.transaction.Transactional;

@Component
public class Apprunner implements ApplicationRunner {

    private final AthleteService athleteService;
    private final AthleteRepository athleteRepository;
    private final CoachRepository coachRepository;

    public Apprunner(AthleteService athleteService, AthleteRepository athleteRepository, CoachRepository coachRepository) {
        this.athleteService = athleteService;
        this.athleteRepository = athleteRepository;
        this.coachRepository = coachRepository;
    }

    @Override
    @Transactional
    public void run(ApplicationArguments args) throws Exception {
        System.out.println(athleteService.findById(1));
//        System.out.println(athleteRepository.findById(2).get());
//        System.out.println(athleteService.findById(2));
//        update();
//        athleteService.setCoach(2, 1);
    }

//    public void update() {
//        Athlete athlete = athleteRepository.findById(2).get();
//        Coach coach = coachRepository.findById(1).get();
//        athlete.setCoach(coach);
//        athleteRepository.save(athlete);
//        System.out.println("Coach ID: " + athlete.getCoach().getId());
//    }
}
