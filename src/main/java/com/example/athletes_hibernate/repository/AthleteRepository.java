package com.example.athletes_hibernate.repository;

import com.example.athletes_hibernate.model.Athlete;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface AthleteRepository extends JpaRepository<Athlete, Integer> {
}
