package com.example.athletes_hibernate.repository;

import com.example.athletes_hibernate.model.Coach;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoachRepository extends JpaRepository<Coach, Integer> {
}
