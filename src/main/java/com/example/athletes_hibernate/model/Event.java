package com.example.athletes_hibernate.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Set;

@Entity
//@Data
@Getter
@Setter
public class Event {
    @Id
    private Integer id;

    @Column(name = "event_name", length = 100, nullable = false)
    private String name;

    @ManyToMany(mappedBy = "events")
    private Set<Athlete> athletes;
}
