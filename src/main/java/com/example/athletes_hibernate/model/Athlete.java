package com.example.athletes_hibernate.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
//@Data
@Getter
@Setter
public class Athlete {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)     //generates the primary key relying on the database auto-increment

    @Column(name = "athl_id")                   //custom name can be given
    private Integer id;

    @Column(name = "athl_name", length = 50, nullable = false)      //additional custom settings
    private String name;

    @ManyToOne
    @JoinColumn(name = "coach_id")
    private Coach coach;

    @ManyToMany
    @JoinTable(
            name = "athlete_event",                               //kapcsolótábla neve
            joinColumns = {@JoinColumn(name = "athl_id")},       //saját oszlop neve
            inverseJoinColumns = {@JoinColumn(name = "event_id")} //kapcsolt oszlop neve
    )
    private Set<Event> events;

    @Override
    public String toString() {
        return "Id: " + this.id + ", Name: " + this.name;
    }
}
