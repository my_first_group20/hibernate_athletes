package com.example.athletes_hibernate.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
//@Data
public class Coach {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 50, nullable = false)
    private String name;

    @OneToMany(mappedBy = "coach")
    private Set<Athlete> athletes;

    @ManyToMany
    @JoinTable(
            name = "coach_certificate",                               //kapcsolótábla neve
            joinColumns = {@JoinColumn(name = "coach_id")},       //saját oszlop neve
            inverseJoinColumns = {@JoinColumn(name = "cert_id")} //kapcsolt oszlop neve
    )
    private Set<Certificate> certificates;
}
