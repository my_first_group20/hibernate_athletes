package com.example.athletes_hibernate.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
//@Data
@Getter
@Setter
public class Certificate {
    @Id
    private Integer id;

    @Column(name = "cert_name", length = 100, nullable = false)
    private String name;

    @ManyToMany(mappedBy = "certificates")
    private Set<Coach> coaches;
}
