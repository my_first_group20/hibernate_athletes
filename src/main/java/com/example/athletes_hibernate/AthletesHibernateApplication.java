package com.example.athletes_hibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AthletesHibernateApplication {

    public static void main(String[] args) {
        SpringApplication.run(AthletesHibernateApplication.class, args);
    }

}
