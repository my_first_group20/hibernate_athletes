package com.example.athletes_hibernate.service;

import com.example.athletes_hibernate.model.Athlete;

public interface AthleteService extends CRUDService<Athlete, Integer> {
        void setCoach(int athleteId, int coachId);
}
