package com.example.athletes_hibernate.service;

import com.example.athletes_hibernate.model.Athlete;
import com.example.athletes_hibernate.model.Coach;
import com.example.athletes_hibernate.repository.AthleteRepository;
import com.example.athletes_hibernate.repository.CoachRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class AthleteServiceImpl implements AthleteService{
    private final AthleteRepository athleteRepository;
    private final CoachRepository coachRepository;
    //private final Logger logger = LoggerFactory.getLogger(AthleteServiceImpl.class);

    public AthleteServiceImpl(AthleteRepository athleteRepository, CoachRepository coachRepository) {
        this.athleteRepository = athleteRepository;
        this.coachRepository = coachRepository;
    }

    @Override
    public void setCoach(int athleteId, int coachId) {
        Athlete athlete = athleteRepository.findById(athleteId).get();
        Coach coach = coachRepository.findById(coachId).get();
        athlete.setCoach(coach);
        athleteRepository.save(athlete);
        System.out.println("Coach ID: " + athlete.getCoach().getId());
    }

    @Override
    public Athlete findById(Integer id) {
        return  athleteRepository.findById(id).get();
    }

    @Override
    public Collection<Athlete> findAll() {
        return null;
    }

    @Override
    public Athlete add(Athlete entity) {
        return null;
    }

    @Override
    public Athlete update(Athlete entity) {
        return null;
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public void delete(Athlete entity) {

    }
}
